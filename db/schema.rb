# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150208231153) do

  create_table "hotels", force: true do |t|
    t.string   "name"
    t.integer  "price"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "room_id"
    t.string   "address"
    t.string   "phone"
    t.integer  "zp"
    t.string   "city"
    t.string   "state"
    t.string   "country"
  end

  create_table "orders", force: true do |t|
    t.integer  "qty"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "room_id"
    t.integer  "user_id"
    t.integer  "discount",                              default: 0
    t.string   "discount_type"
    t.integer  "kids"
    t.decimal  "paid",          precision: 8, scale: 2
    t.integer  "days",                                  default: 1
  end

  add_index "orders", ["room_id"], name: "index_orders_on_room_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "payments", force: true do |t|
    t.integer  "amount"
    t.string   "payment_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "order_id"
  end

  add_index "payments", ["order_id"], name: "index_payments_on_order_id", using: :btree

  create_table "room_images", force: true do |t|
    t.string   "image"
    t.integer  "room_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  create_table "rooms", force: true do |t|
    t.string   "name"
    t.string   "type"
    t.integer  "cap"
    t.integer  "quantity"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "start_time",                          default: ""
    t.string   "end_time"
    t.text     "services"
    t.string   "description"
    t.string   "room_view"
    t.string   "beds"
    t.decimal  "price",       precision: 7, scale: 2
    t.integer  "hotel_id"
    t.integer  "order_id"
    t.string   "room_type"
    t.string   "image"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "order_id"
    t.integer  "control_id"
    t.integer  "days"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["order_id"], name: "index_users_on_order_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
