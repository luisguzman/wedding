class AddDaysToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :days, :integer, default: 1
  end
end
