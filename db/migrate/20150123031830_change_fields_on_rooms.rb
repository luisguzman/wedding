class ChangeFieldsOnRooms < ActiveRecord::Migration
  def change
    change_column :rooms, :start_time, :string
    change_column :rooms, :end_time, :string
  end
end
