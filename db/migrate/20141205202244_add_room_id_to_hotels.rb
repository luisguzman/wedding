class AddRoomIdToHotels < ActiveRecord::Migration
  def change
    add_column :hotels, :room_id, :integer
  end
end
