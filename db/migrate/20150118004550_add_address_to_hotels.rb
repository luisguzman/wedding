class AddAddressToHotels < ActiveRecord::Migration
  def change
    add_column :hotels, :address, :string
    add_column :hotels, :phone, :string
    add_column :hotels, :zp, :integer
    add_column :hotels, :city, :string
    add_column :hotels, :state, :string
    add_column :hotels, :country, :string
  end
end
