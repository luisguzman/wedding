class ChangeColumnOnRooms < ActiveRecord::Migration
  def change
    change_column :rooms, :services, :text
  end
end
