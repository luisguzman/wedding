class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :name
      t.string :type
      t.integer :cap
      t.integer :quantity

      t.timestamps
    end
  end
end
