class AddKidsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :kids, :integer
  end
end
