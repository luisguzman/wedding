class AddStartTimeToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :start_time, :date
    add_column :rooms, :end_time, :date
    add_column :rooms, :services, :string
    add_column :rooms, :description, :string
    add_column :rooms, :room_view, :string
    add_column :rooms, :beds, :string
    add_column :rooms, :price, :decimal, precision: 7, scale: 2
  end
end
