class AddRoomRefToOrders < ActiveRecord::Migration
  def change
    add_reference :orders, :room, index: true
  end
end
