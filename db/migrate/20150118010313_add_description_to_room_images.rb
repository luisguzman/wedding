class AddDescriptionToRoomImages < ActiveRecord::Migration
  def change
    add_column :room_images, :description, :string
  end
end
