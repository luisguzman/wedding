class AddOrderIdToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :order_id, :integer
  end
end
