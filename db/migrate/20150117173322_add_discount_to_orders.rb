class AddDiscountToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :discount, :integer
    add_column :orders, :discount_type, :string
  end
end
