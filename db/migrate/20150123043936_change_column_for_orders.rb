class ChangeColumnForOrders < ActiveRecord::Migration
  def change
    change_column :orders, :discount, :integer, default: 0
  end
end
