Rails.application.routes.draw do
  
  devise_for :users, :controllers => { :registrations => "users/registrations" }

  devise_scope :users do
    get '/login' => 'devise/sessions#new'
    get '/salir' => 'devise/sessions#destroy'
  end
  # resources :user, :controller => "user"
  root :to => "static#index"
  get 'index' => "static#index2"

  get 'la-propuesta' => 'static#our_story', as: 'our_story'
  get 'informacion' => 'static#info', as: 'info'
  
  resources :users, only: [:show, :edit]
  get 'mi-cuenta' => 'users#show', as: 'my_account'


  resources :hotels, path: 'hoteles', only: [:index, :show] do
    resources :rooms, path: 'habitaciones', only: [:index, :show]
  end
  resources :galleries, path: 'galerias', only: [:index, :show]

  resources :photos, only: [] do
    collection do
      get :tags
    end
  end

  get 'oauth/callback' => 'photos#callback'
  get 'oauth/connect' => 'photos#connect'

  get 'admin' => 'admin#dashboard'

  get 'static/index'

  namespace :admin do
    resources :users
    resources :hotels do
      resources :rooms
    end
    resources :galleries
    resources :orders do
      put :pay, on: :member
      delete 'destroy_payment/:id' => 'orders#destroy_payment', as: 'destroy'
    end
  end


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
