# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  initialize = ->
    mapCanvas = document.getElementById('map-canvas');

    mapOptions = {
      center: new google.maps.LatLng(20.254082, -87.403651),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.SATELLITE
    }
    map = new google.maps.Map(mapCanvas, mapOptions)
  google.maps.event.addDomListener(window, 'load', initialize)

  $('#countdown').countdown({ until: new Date(2015, 9-1, 18), layout: '<div>{y<}<span class="countdown-item"><span class="number">{yn}</span> {yl}</span>{y>}{o<}<span class="countdown-item"><span class="number">{on}</span> {ol}</span>{o>}' + 
    '{d<}<span class="countdown-item"><span class="number">{dn}</span> {dl}</span>{d>}{h<}<span class="countdown-item"><span class="number">{hn}</span> {hl}</span>{h>}' + 
    '{m<}<span class="countdown-item"><span class="number">{mn}</span> {ml}</span>{m>}{s<}<span class="countdown-item"><span class="number">{sn}</span> {sl}</span>{s>}</div>' })

  $('#room_start_time').timepicker()
  $('#room_end_time').timepicker()

  $('#nav').onePageNav ->
    currentClass: 'current',
    changeHash: false,
    scrollSpeed: 750,
    scrollThreshold: 0.5,
    filter: '',
    easing: 'swing',
    begin: ->
        #//I get fired when the animation is starting
    ,
    end: ->
        #//I get fired when the animation is ending
    ,
    scrollChange: -> ($currentListItem)
        #//I get fired when you enter a section and I pass the list item of the section
