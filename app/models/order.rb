class Order < ActiveRecord::Base
  belongs_to :room
  belongs_to :user
  has_many :payments

  validates_presence_of :room, :user

  before_save :validate_data

  def total_amount
    @total = room.price * total_guests * days
    @total = @total + (kids*800 * days) if !kids.nil? && kids > 0
    @total = @total - discount_total if !discount.nil? && discount > 0
    return 0 if @total.nil?
    return @total
  end

  def discount_total
    if discount_type == 'percent'
      (room.price * total_guests * days)/100 * discount
    else
      discount
    end
  end

  def total_paid
    payments.sum(:amount) || paid || 0
  end

  def total_guests
    return qty - kids if kids > 0
    qty
  end

  def validate_data
    self.discount = 0 if discount.nil?
    self.kids = 0 if kids.nil?
  end

end
