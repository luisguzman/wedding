class Room < ActiveRecord::Base
  belongs_to :hotel 
  has_many :order
  mount_uploader :image, ImageUploader
end
