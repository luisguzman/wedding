class PhotosController < ApplicationController
  CALLBACK_URL = "http://localhost:3000/oauth/callback"

  def connect
    url = Instagram.authorize_url(:redirect_uri => CALLBACK_URL)
    redirect_to url
  end

  def callback
    redirect_to '/photos'
  end

  def tags
    client = Instagram.client(:access_token => session[:access_token])
    tags = client.tag_search('LilianaIsaacBeachWedding2015')
    page = params.has_key?(:next_max_tag_id) ? params[:next_max_tag_id] : 0
    @photos = []
    if !tags.count.zero?
      @photos = page.to_i.zero? ? Instagram.tag_recent_media(tags[0].name) : Instagram.tag_recent_media(tags[0].name, :max_tag_id => params[:next_max_tag_id])
      @next_max_tag_id = @photos.pagination.next_max_tag_id
    end
  end

end
