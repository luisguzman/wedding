class HotelsController < ApplicationController
  before_action :set_hotel, only: [:show, :rooms]
  before_action :authenticate_user!

  # GET /hotels
  # GET /hotels.json
  def index
    @hotels = Hotel.all
  end

  # GET /hotels/1
  # GET /hotels/1.json
  def show
  end

  def rooms
    @rooms = @hotel.rooms
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hotel
      @hotel = Hotel.find(params[:id])
    end

end
