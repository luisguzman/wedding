class AdminController < ApplicationController
  respond_to :html
  before_filter :authenticate_admin, :except => :login
  #load_and_authorize_resource User
  layout 'admin'

  def dashboard
    redirect_to admin_users_path
  end

  private 
  def authenticate_admin
    redirect_to '/users/sign_in' unless user_signed_in?

  end

end
