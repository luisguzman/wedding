class Admin::RoomsController < ApplicationController
  before_filter :set_room, only: [:edit, :update, :destroy, :show]
  layout 'admin'

  # GET /rooms
  # GET /rooms.json
  def index
    @rooms = Room.all
  end

  def show
    
  end
  
    # GET /rooms/new
  def new
    @room = Room.new
  end

  # GET /rooms/1/edit
  def edit
  end

  # POST /rooms
  # POST /rooms.json
  def create
    hotel = Hotel.find(params[:hotel_id])
    @room = hotel.rooms.build(room_params)

    respond_to do |format|
      if @room.save
        format.html { redirect_to admin_hotel_path(hotel), notice: 'room was successfully created.' }
        format.json { render :show, status: :created, location: @room }
      else
        format.html { render :new }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to admin_hotel_path(id: @room.hotel.id), notice: 'room was successfully updated.' }
        format.json { render :show, status: :ok, location: @room }
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  # DELETE /rooms/1.json
  def destroy
    hotel = @room.hotel
    @room.destroy
    respond_to do |format|
      format.html { redirect_to admin_hotel_url(hotel), notice: 'room was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_room
      @room = Room.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def room_params
      params.require(:room).permit(:name, :price, :description, :cap, :room_type, :start_time, :end_time, :services, :room_view, :beds, :image, :remote_image_url)
    end
end
