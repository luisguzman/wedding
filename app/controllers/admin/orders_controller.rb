class Admin::OrdersController < ApplicationController
  before_filter :set_order, only: [:show, :edit, :update, :destroy, :pay]
  before_filter :set_rooms, only: [:new, :edit]
  layout 'admin'
  include ActionView::Helpers::NumberHelper

  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
  end
  
    # GET /orders/new
  def new
    @order = Order.new(user_id: params[:user_id])
  end

  # GET /orders/1/edit
  def edit
  end

  def pay
    @payment = @order.payments.new(payment_params)
    respond_to do |format|
      if @payment.save
        format.html { redirect_to admin_order_path, notice: 'Pago registrado.' }
        format.js { render :pay }
      else
        format.html { render :show }
        format.js { render :pay }
      end
    end
  end

  def destroy_payment
    @payment = Payment.find(params[:id])
    @order=Order.find(params[:order_id])
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to admin_user_url(@order.user), notice: 'Pago eliminado.' }
      format.json { head :no_content }
    end
  end

  # POST /orders
  # POST /orders.json
  def create
    user = User.find(params[:order][:user_id])
    @order = Order.new(order_params)
    user.order = @order
    user.save!

    respond_to do |format|
      if @order.save
        format.html { redirect_to admin_users_path, notice: 'order was successfully created.' }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to admin_users_path, notice: 'order was successfully updated.' }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to admin_hotel_order_url(@order), notice: 'order was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    def set_rooms
      @rooms_available = Room.all.collect{|r| ["#{r.name} - #{number_to_currency(r.price)}", r.id] }
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:user_id, :qty, :discount, :discount_type, :room_id, :kids, :days, :paid)
    end

    def payment_params
      params.require(:payment).permit(:amount, :payment_type)
    end
end
