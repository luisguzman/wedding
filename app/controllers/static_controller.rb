class StaticController < ApplicationController
  layout 'homepage', only: :index
  before_action :authenticate_user!, except: [:index]
  
  def index
    return redirect_to index_path if user_signed_in?
  end

  def our_story
  end

  def info
  end

  def index2
  end
end
