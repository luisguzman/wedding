require 'rails_helper'

RSpec.describe "hotels/index", :type => :view do
  before(:each) do
    assign(:hotels, [
      Hotel.create!(
        :name => "Name",
        :price => 1
      ),
      Hotel.create!(
        :name => "Name",
        :price => 1
      )
    ])
  end

  it "renders a list of hotels" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
